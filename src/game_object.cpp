#include "../include/game_object.h"		//	The game_obj class
#include "../include/loadTex.h"			// loading textures easily
#include <iostream>
#include <stdlib.h>  // srand



// +--------------------------------------------------------------------+
// |This file should contain all the player logic, update, render stuff |
// +--------------------------------------------------------------------+



//		+---------------+
//		|	CONSTRUCTOR	|
//		+---------------+
GameObject::GameObject(const char* texture, SDL_Renderer* ren, bool ghost, int xp, int yp, int dir) {
	enemy = ghost;
	if (!enemy)
		stopped = true;
	else
		stopped = false;
	pushed = true;
	go_ren = ren;										// point to renderer	
	go_texture = TexLoad::getTexture(texture, go_ren);	//	load texture like pacman
	xPos = xp; yPos = yp;
	direction = dir;
	if (!enemy)animationFrame = 0;
	else animationFrame = 4;		// something else

}
// test

//		+-------------------+
//		|	DESTRUCTOR :'(	|
//		+-------------------+
GameObject::~GameObject(){	}


//		+-----------------------+
//		|	UPDATE THE OBJECT	|
//		+-----------------------+
void GameObject::updatePacman(int dir) {

//	Pacman gets controlled, but not the ghosts! 

	

	std::cout << floor(xPos/SPRITESIZE) << '\n';
	
					//	IF it is PACMAN
	direction = dir;
	
	if (!stopped) {
		switch (direction) {
			case 0: yPos++; break;
			case 1: yPos--; break;
			case 2: xPos--; break;
			case 3: xPos++; break;
		}
		
	}
	

	if(!pushed) 
		switch (direction) {
		case 0: yPos--; break;
		case 1: yPos++; break;
		case 2: xPos++; break;
		case 3: xPos--; break;
		}
	pushed = true;

	if (animationFrame >= 4) animationFrame = 0;		// reset animation

	sRect.h = SPRITE;				//	Animation with SPRITE = 72
	sRect.w = SPRITE;
	sRect.x = 0 + SPRITE*animationFrame;
	sRect.y = 0 + SPRITE*direction;

	animationFrame++;

	dRect.x = xPos;
	dRect.y = yPos;
	dRect.h = SPRITESIZE;		// These determine the size i guess
	dRect.w = SPRITESIZE;		// 
	
	
}

void GameObject::updateGhost() {
								//	If it is ghost!


	if (!pushed)
		switch (direction) {
		case 0: yPos--; pushed = true; break;
		case 1: yPos++; pushed = true; break;
		case 2: xPos++; pushed = true; break;
		case 3: xPos--; pushed = true; break;
		}
	//direction = 1;				// test direction will be AI-ed
	if(!stopped)
		switch (direction) {
		case 0: yPos++; break;
		case 1: yPos--; break;
		case 2: xPos--; break;
		case 3: xPos++; break;
		}
	


	sRect.h = SPRITE;		// Frame selection and animation for GHOST!
	sRect.w = SPRITE;
	sRect.x = SPRITE * animationFrame++; 
	sRect.y = 0 + SPRITE * direction;

	if (animationFrame > 5)animationFrame = 4;

	dRect.x = xPos;
	dRect.y = yPos;
	dRect.h = SPRITESIZE;		// These determine the size i guess
	dRect.w = SPRITESIZE;		// 
	
}


//		+-----------------------+
//		|	RENDER THE OBJECT	|
//		+-----------------------+
void GameObject::render() {
				// ren		image		src,	DESTINATION 
	SDL_RenderCopy(go_ren, go_texture, &sRect, &dRect);
}




SDL_Rect GameObject::rec() {
	return dRect;
}

void GameObject::pushAfterCollision() {
	pushed = false;
	
}

void GameObject::stop(bool stop) {
	stopped = stop;
}

void GameObject::reset(int startX, int startY) {
	stopped = true;
	pushed = true;
	dRect.x = startX; dRect.y = startY;
	xPos = startX; yPos = startY;
}

void GameObject::divert() {
	if (stopped)direction = rand() % 4;
	//if (direction > 3) direction = 0;
}

int GameObject::xTile()
{
	return floor(dRect.x / SPRITESIZE);
}


int GameObject::yTile()
{
	return floor(dRect.y / SPRITESIZE);
}