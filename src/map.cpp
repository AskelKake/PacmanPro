#include "../include/Map.h"			
#include "../include/Constants.h"	// Sprite sizes


#include <iostream>






Map::Map(SDL_Renderer* ren, int xp, int yp)
{
	map_ren = ren;
	walls = TexLoad::getTexture("Assets/Wall.png", map_ren);
	food = TexLoad::getTexture("Assets/food.png", map_ren);
	eaten = TexLoad::getTexture("Assets/no_food.png", map_ren);

	//LoadMap(lvl1);
	//LoadMapFromFile("level0.txt");
	LoadMap("level0.txt");

	src.x = src.y = 0;
	src.w = 100;
	src.h = 100;
	dest.w = SPRITESIZE;
	dest.h = SPRITESIZE;

	

	
}

Map::~Map()
{


}


//	Read in the file
void Map::LoadMap(std::string filePath) {
	
	std::ifstream MapFile(filePath);
	

	
	if (MapFile) 
		for (int i = 0; i < 36; i++) 
			for (int j = 0; j < 28; j++) 
				MapFile >> map[i][j]; 	

	else std::cout << "Can't load map\n";
	

}



void Map::updateMap(int i, int j)
{
	if(map[i][j] != 1 )
		map[i][j] = 3;
}



// rendering the map
void Map::DrawMap()
{
	
	int type = 0;
	//type = map[0][0];
	//type = map[31][28];
	
	for (int row = 0; row < 36; row++) {

		for (int column = 0; column < 28; column++)
		{
			type = map[row][column];
			dest.x = column * SPRITESIZE;
			dest.y = row * SPRITESIZE;

			
			// wall for 1 and food for 0
			switch (type)
			{
			case 1:
				TexLoad::Draw(map_ren, walls,  src, dest);
				
				break;
			case 0:
				TexLoad::Draw(map_ren, food, src, dest);
				break;
			default:
				TexLoad::Draw(map_ren, eaten, src, dest); 
			
				break;
			}
			
		}
	}
}


void Map::getRect(std::vector<SDL_Rect> &rectList, std::vector<SDL_Rect> &foodList) {
	int type = 0;

	

	for (int row = 0; row < 36; row++) {
		

		for (int column = 0; column < 28; column++)
		{
			
			type = map[row][column];
			dest.x = column * SPRITESIZE;
			
			dest.y = row * SPRITESIZE;

			switch (type)
			{
			case 1:
				rectList.push_back(dest);		// getting all the rects in the vector list
				

				break;
			case 0:
				foodList.push_back(dest);
				

				break;
			default:

				break;
			}

		}
	}

}
