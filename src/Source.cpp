// C++ Headers
#include <string>
#include <iostream>
#include <stdlib.h>

// OpenGL / glew Headers
//#define G L3_PROTOTYPES 1        this caused IDE errors but still works though

#ifdef _WIN32 // Windows 10 (Visual Studio)

#endif

#include "../Dependencies/SDL2-2.0.7/include/SDL.h"
#include "../Dependencies/SDL2-2.0.7/include/SDL_image.h"
#include "../include/Constants.h"
#include "../include/game_object.h"
#include "../include/map.h"
#include <vector>			// for vector list
#include "../include/loadTex.h"


//		+---------------------------+
//		|	FUNC DECLARATIONS		|
//		+---------------------------+
bool initGame();
void update();
void pac_checkCollideGhost();		// if pacman is about to die from ghosts
void ghost_checkCollideWall();		// ghosts detect walls
void pac_CheckCollideWall();		// check Pacman collide with walls
void rendering();
void clearGame();
void eventHandler();
void keyBoardHandler();
void pac_eatFood();


//static void addTile(int id, int x, int y); 

bool collision(SDL_Rect r1, SDL_Rect r2);




//		+-------------------------------------------+
//		|	SOME GLOBAL POINTERS AND VARIABLES		|
//		+-------------------------------------------+


GameObject* player;					// pacman
GameObject* ghostTest[MAXGHOST];	// ghost				
SDL_Window* window;					// window
Map* map;							// map
SDL_Renderer* ren = nullptr;		// render



SDL_Event event;			// listen to events


bool runs;					//	the game is running
bool paused;

int dir;					//	player direction
bool stopped;				// pacman stopped
int lives = 10;				// lives
int score = 0;				// score




std::vector<SDL_Rect> mapRects;		// is going to contain all dRects for the map things, for collision
std::vector<SDL_Rect> food;			// food on 0- tiles



Uint32 frameStart;
int frameTime;


//		+-------------------+
//		|		MAIN!		|
//		+-------------------+
int main(int argc, char *argv[]) {

	//		+---------------+
	//		|	INIT GAME	|
	//		+---------------+
	
	if (!initGame()) std::cout << "well... **** it did not init anything!\n";

	

	//		+-------------------+
	//		|	PLAYER, LIVE!	|
	//		+-------------------+

	player = new GameObject("Assets/pacman.png", ren, false, STARTPOS_X, STARTPOS_Y, dir);
	ghostTest[0] = new GameObject("Assets/pacman.png", ren, true, 6*SPRITESIZE, 20*SPRITESIZE, 3);
	ghostTest[1] = new GameObject("Assets/pacman.png", ren, true, 26*SPRITESIZE, 5*SPRITESIZE, 3);
	ghostTest[2] = new GameObject("Assets/pacman.png", ren, true, 18*SPRITESIZE, 8*SPRITESIZE, 2);
	
	map = new Map(ren, 0, 0);
	map->getRect(mapRects, food);		// push all the dRects to the list to get collision bodies
	

	

	//		+-------------------+
	//		|	GAME LOOP		|
	//		+-------------------+
	runs = true;

	while (runs) {	
		
		frameStart = SDL_GetTicks();			// Nevermind this, just for frames	

		// ALL UPDATE AND RENDER STUFF GO IN THAT FUNCSJONS!
		eventHandler();
		if(!paused)update();
		rendering();
		

		//		This is for frames too so never mind, again
		frameTime = SDL_GetTicks() - frameStart;
		if (FDL > frameTime) SDL_Delay(FDL - frameTime);	
	}


	
	//		+-------------------+
	//		|	CLEAR			|
	//		+-------------------+
	clearGame();		
	delete player;
	for(int i = 0; i < MAXGHOST; i++)
		delete ghostTest[i];
	
	return 0;
}

void menuScreen() {

}

//
//		+---------------------------+
//		|	ALL RENDERING STUFF		|
//		+---------------------------+
void rendering() {
	SDL_RenderClear(ren);
	//		Render stuff between here
	map->updateMap(player->yTile(), player->xTile());
	map->DrawMap();
	player->render();
	
	for(int i = 0; i < MAXGHOST; i++)
		ghostTest[i]->render();
	SDL_RenderPresent(ren);
}


//		+-------------------------------+
//		|	ALL TO CLEAR THE GAME		|
//		+-------------------------------+
void clearGame() {
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(ren);
	std::cout << "game cleared\n";
}




//		+-------------------+
//		|	ALL UPDATES		|
//		+-------------------+
void update() {
	pac_CheckCollideWall();			// See if Pacman collides with walls
	player->updatePacman(dir);		// update player [ WORKS ]
	
	pac_eatFood();
	pac_checkCollideGhost();		// see if Pacman collides with a ghost
	ghost_checkCollideWall();		// see if ghosts collide with walls and update them
	
}

void pac_checkCollideGhost() {

	for (int i = 0; i < MAXGHOST; i++)

		if (collision(player->rec(), ghostTest[i]->rec())) {		// collision works!
			//std::cout << "me touch ghost!\n";
			player->stop(true);
			player->pushAfterCollision();
			lives--;
			std::cout << "lives: " << lives << '\n';
			player->reset(STARTPOS_X, STARTPOS_Y );
		}
}

void pac_CheckCollideWall() {
	for (int i = 0; i < mapRects.size(); i++)
		if (collision(player->rec(), mapRects[i])) {		// collision works!
																	std::cout << "me touch wall!\n";
			player->stop(true);
			player->pushAfterCollision();
			//player->reset(STARTPOS_X, STARTPOS_Y );
		}

}

void pac_eatFood() {
	for (int i = food.size()-1; i >= 0; i--)
		if (collision(player->rec(), food[i])) {		// collision works!
			score++;
			std::cout << "Eating food! Score: " << score << '\n';
			food.erase(food.begin()+i);
			
			//	
			//player->reset(STARTPOS_X, STARTPOS_Y );
		}

}

//		+-------------------------------------------+
//		|	              A.I!						|
//		+-------------------------------------------+


//	Basically this function checks if any ghost touches any wall. And if it does, it diverts course
void ghost_checkCollideWall() {
	for (int i = 0; i < MAXGHOST; i++) {

		ghostTest[i]->stop(false);

		for(int j = 0; j < mapRects.size(); j++)
			if (collision(ghostTest[i]->rec(), mapRects[j])) {
				ghostTest[i]->stop(true);						//	if it is a wall only	
				ghostTest[i]->pushAfterCollision();				// if it is a wall only
			}

		ghostTest[i]->updateGhost();						// update  them	
		ghostTest[i]->divert();	
	}	
}



//		+-------------------------------------------+
//		|	PREPARE MOST THINGS FOR THE GAME		|
//		+-------------------------------------------+
bool initGame() {

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		std::cout << "couldnt init anything for SDL\n";
		return false;								            // faulty exit 
	}




	//		Creating the window
	window = SDL_CreateWindow("The Real Pacman", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		WINHEIGHT, WINWIDTH, false);



	//		Creating the renderer
	ren = SDL_CreateRenderer(window, -1, 0);
	if (!ren) {
		std::cout << "could not create renderer\n";
		return false;
	}



	//	Init the image loader for PNG
	if (!IMG_Init(IMG_INIT_PNG) & (IMG_INIT_PNG)) {
		std::cout << "Failed to init image loader. Remember the SDL_image.dll in root folder!";
		return false;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	
	return true;
	paused = false;
}

void eventHandler() {
	SDL_PollEvent(&event);

	switch (event.type)
	{
		case SDL_QUIT: runs = false; break;
		case SDL_KEYDOWN: keyBoardHandler();
		default:
			break;
	}
}

void keyBoardHandler() {
	switch (event.key.keysym.sym) {
		case SDLK_s:	dir = 0; player->stop(false); break;
		case SDLK_w:	dir = 1; player->stop(false); break;
		case SDLK_a:	dir = 2; player->stop(false); break;
		case SDLK_d:	dir = 3; player->stop(false); break;
		case SDLK_ESCAPE:  paused = !paused;  break;
		

	}

	
}


// Collision stuff



//	THe big collision Q_Q
bool collision(SDL_Rect r1, SDL_Rect r2) {
	
	if (
		(
			r1.x < r2.x + r2.w &&
			r1.x + r1.w > r2.x &&
			r1.y < r2.y + r2.h &&
			r1.y + r1.h > r2.y
		)
		
		) 
		
		return true;
	return false;
}

