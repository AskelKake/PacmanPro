#include "../include/loadTex.h"


//		This function is responsible to give out textures

SDL_Texture* TexLoad::getTexture(const char* file, SDL_Renderer* ren) {
	SDL_Surface* tmpSurf = IMG_Load(file);
	SDL_Texture* tmpTex = SDL_CreateTextureFromSurface(ren, tmpSurf);
	SDL_FreeSurface(tmpSurf);

	return tmpTex;
}

void TexLoad::Draw(SDL_Renderer* ren, SDL_Texture* tex, SDL_Rect src, SDL_Rect dest)
{
	
	SDL_RenderCopy(ren, tex, &src, &dest);

}

