#ifndef _TEX_LOAD
#define _TEX_LOAD

#include "../Dependencies/SDL2-2.0.7/include/SDL.h"
#include "../Dependencies/SDL2-2.0.7/include/SDL_image.h"
#include "../include/map.h"
class TexLoad {

public:
	static SDL_Texture* getTexture(const char* file, SDL_Renderer* ren);
	static void Draw(SDL_Renderer* ren, SDL_Texture* tex, SDL_Rect src, SDL_Rect dest);
};


	
#endif