#pragma once

//#include "../include/Constants.h"


#include "../include/loadTex.h"
#include<iostream>
#include <istream>
#include <fstream>
#include <string>
#include <vector>

						
// include these instead
//  #include "../Dependencies/SDL2-2.0.7/include/SDL.h"
//  #include "../Dependencies/SDL2-2.0.7/include/SDL_image.h"



class Map
{
public:
	Map(SDL_Renderer* ren, int xp, int yp);
	~Map();

	void updateMap(int i, int j);
	void LoadMap(std::string filePath);
	void DrawMap();
	void getRect(std::vector<SDL_Rect> &rectList, std::vector<SDL_Rect> &foodList);
	SDL_Renderer* map_ren;

private:
	SDL_Rect src, dest;
	SDL_Texture* walls;
	SDL_Texture* food;
	SDL_Texture* eaten;


	SDL_Texture* map_tex;

	int map[36][28];
	int width, height;

};

