#ifndef _GAME_OBJ
#define _GAME_OBJ

#include "../Dependencies/SDL2-2.0.7/include/SDL.h"
#include "../Dependencies/SDL2-2.0.7/include/SDL_image.h"
#include "../include/Constants.h"


class GameObject {
private:
	int xPos;
	int yPos;
	int direction;
	bool stopped;
	bool pushed;
	int animationFrame;
	bool enemy;
	SDL_Renderer* go_ren;		//	will point to the same renderer
	SDL_Rect sRect, dRect;		//	source rect and destination rect
	SDL_Texture* go_texture;	//	for texture

public:
	GameObject(const char* texturePath, SDL_Renderer* ren, bool ghost, int xp, int yp, int dir);		// texturePath with renderer
	~GameObject();
	void updatePacman(int dir);
	void stop(bool stop);
	void updateGhost();
	void divert();
	void render();
	void pushAfterCollision();
	void reset(int startX, int startY);
	int xTile();
	int yTile();
	

	SDL_Rect rec();
};


#endif